#!/usr/bin/env bats

load test_helper

@test "Create dotfile(s)" {
  run rcup -d infrastructure/dotfiles
  [ "$status" -eq 0 ]
  grep "[alias]" $HOME/.gitconfig
  run rcdn -d infrastructure/dotfiles
  [ ! -f $HOME/.gitconfig ]
}
