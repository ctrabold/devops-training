#!/usr/bin/env bats

load test_helper

@test "Override HOME for tests" {
  run echo $HOME
  [ "$status" -eq 0 ]
  echo "$output" | grep tmp/home$
}

@test "Requires Mac OS" {
  export OSTYPE=ubuntu
  run bash ./bin/setup.sh
  [ "$status" -eq 1 ]
  echo "$output" | grep "WARN"
}
