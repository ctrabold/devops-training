#!/bin/bash
# This script installs required software

set -eu

# Smoketest environment
if [[ $OSTYPE != darwin* ]]; then
  echo "WARN Only MacOS is supported at the moment."
  echo "WARN Please install the tools manually on other operation systems."
  exit 1
fi

if [[ `which python` ]]; then
  echo "OK Found python!"
else
  echo "WARN Please install python first."
fi

if [[ $OSTYPE == darwin* ]]; then
  if [[ `which xcode-select` ]]; then
    echo "OK Found xcode-select!"
  else
    echo "WARN Please install XCode first."
    echo "The most common issue is that the Command Line Tools (CLT) for Xcode are missing."
    echo "If possible install Xcode and the CLT from a fast network! It's a huge download."
    echo "Follow these instructions http://docs.brew.sh/Installation.html#requirements"
    exit 1
  fi

  if [[ `which brew` ]]; then
    echo "OK Found homebrew!"
  else
    echo "INFO Installing homebrew"
    ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
  fi

  if [[ ! `brew bundle --help` ]]; then
    echo "INFO Installing brew bundle (https://github.com/Homebrew/homebrew-bundle)"
    brew tap Homebrew/bundle
  else
    echo "OK Found 'brew bundle' command"
  fi

  echo "Cleanup old 'homebrew' packages to free disk space"
  brew prune && brew cleanup && brew tap --repair
fi

if [[ `which ansible` ]]; then
  echo "OK Ansible is already installed"
else
  echo "INFO Installing ansible..."
  if [[ $OSTYPE == darwin* ]]; then
    brew install ansible
  else
    echo "WARN Only MacOS is supported. Exiting"
    exit 1
  fi
fi

if [[ $OSTYPE == darwin* ]]; then
  echo "INFO Install applications that we are going to use"
  brew bundle --verbose
fi
