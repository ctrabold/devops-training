# Overview

This document gets you quickly started for the DevOps workshop.

To make best use of our time, please prepare your computer and have a look at the requirements below.

Ping me if you have questions or need assistance!

Looking forward to it!

Christian

# Requirements

**Currently only MacOS Sierra is fully tested.**

The following requirements need to be fulfilled on every OS:

- Make sure you have 5GB of free disk space (you need space for virtual machines for example)
  Empty `Trash` and cleanup `Downloads` folder is a good start.

- Install git version 2.12.0

- Install vagrant version 1.9.2: https://www.vagrantup.com/downloads.html

- Install virtualbox version 5.1.x: http://download.virtualbox.org/virtualbox/

- Install ansible 2.2.1.0: http://docs.ansible.com/ansible/intro_installation.html

- Terminal / Command line

## Requirements MacOS

- Assuming you are running MacOS Sierra or El Capitan with `XCode` & `Command line tools` installed

  If possible install `Xcode` and the `Command line tools` from a fast network! It's a huge download.
  Follow these instructions http://docs.brew.sh/Installation.html#requirements

## Requirements Windows

- Install Git for Windows: https://git-for-windows.github.io


# Installation

1) Checkout this repository:

    git clone https://gitlab.com/ctrabold/devops-training.git

2) Change into the directory:

    cd devops-training

3) Run this command:

    bash bin/setup.sh

4) Now start the local `centos` VM:

    vagrant up centos

You can also start the `ubuntu` VM.

Do not start both VMs. Otherwise you might experience a significant impact on your host machine
because the VMs use system resources like CPU & RAM from the host system.

5) _Congratulations_ you're all set for the workshop!

# File structure

```
├── Brewfile              - List of homebrew packages
├── README.md             - The file you are reading
├── Vagrantfile           - Configuration file for Vagrant; setup local VMs
├── ansible.cfg           - Configuration file for Ansible; defines global settings e.g. path to inventory
├── atom-package-list.txt - List of Atom plugins; install with `apm install --packages-file atom-package-list.txt`
├── doc                   - Project documentation
├── infrastructure        - Infrastructure as code
│   ├── ansible           - Ansible roles to setup infrastructure components
│   └── dotfiles          - Dotfiles for local setup
├── bin/                  - Scripts for project related tasks
│   └── setup.sh          - Script to setup local machine
├── test                  - Tests for scripts
└── vendor                - Third party files & tools; not in version control and ignored by git
```

# FAQ

## I can not install `homebrew`

Follow these instructions: http://docs.brew.sh/Troubleshooting.html

The most common issue is that the Command Line Tools (CLT) for Xcode are missing.
If possible install Xcode and the CLT from a fast network! It's a huge download.
Follow these instructions http://docs.brew.sh/Installation.html#requirements

The second common issue is user permissions. You can run this command to fix the permissions:

    cd /usr/local && sudo chown -R $(whoami) bin etc include lib sbin share var Frameworks

## How can I `ssh` into the VMs?

- Make sure that the VMs are up and running

    vagrant status

- Use these `SSH` commands:

    ssh vagrant@192.168.0.10 -i ~/.vagrant.d/insecure_private_key
    ssh vagrant@192.168.0.20 -i ~/.vagrant.d/insecure_private_key

Alternatively you can use `ad-hoc` ansible commands to execute commands via `SSH`:

    ansible all -m ping

## How do I repackage Vagrant VMs?

- List all available boxes

    vagrant box list

- Note down the boxes you'd like to package

    vagrant box repackage ubuntu/trusty64 virtualbox 14.04
    vagrant box repackage centos/7 virtualbox 1609.01

## How do I add boxes to vagrant?

Run these commands:

    vagrant box add --name ubuntu/trusty64 --box-version 14.04   ~/Downloads/ubuntu-14.04.box
    vagrant box add --name centos/7        --box-version 1609.01 ~/Downloads/centos-7.box

## How do I generate a new `ssh` key?

Run this command:

    ssh-keygen -f $PWD/devops-training -C devops-training-example-key


## I have another issue

Ping me if you need assistance: christian.trabold@gmail.com
