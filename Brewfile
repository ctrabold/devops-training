# Install homebrew taps
tap 'caskroom/cask'
tap 'caskroom/fonts'
tap 'caskroom/versions'
tap 'homebrew/binary'
tap 'homebrew/dupes'
tap 'homebrew/versions'
tap 'thoughtbot/formulae'

# Install 'homebrew' packages
brew 'ack'
brew 'ansible'
brew 'autojump'
brew 'bats'
brew 'caddy'
brew 'docker-compose'
brew 'git-extras'
brew 'git'
brew 'htop-osx'
brew 'jq'
brew 'mas'
brew 'packer'
brew 'pstree'
brew 'pwgen'
brew 'rcm'
brew 'rsync'
brew 'shellcheck'
brew 'tig'
brew 'tree'
brew 'vim'
brew 'wget'
brew 'zsh'

# Install apps with brew-cask
cask 'appcleaner'
cask 'atom'
cask 'docker'
cask 'caffeine'
cask 'iterm2'
cask 'kdiff3'
cask 'keeweb'
cask 'keycastr'
cask 'launchcontrol'
cask 'moom'
cask 'vagrant'
cask 'virtualbox'

# Install quicklook plugins with brew-cask (https://github.com/sindresorhus/quick-look-plugins)
cask 'betterzipql'
cask 'provisionql'
cask 'qlcolorcode'
cask 'qlimagesize'
cask 'qlmarkdown'
cask 'qlprettypatch'
cask 'qlstephen'
cask 'qlvideo'
cask 'quicklook-csv'
cask 'quicklook-json'
cask 'suspicious-package'
cask 'webpquicklook'

# Install fonts
cask 'font-meslo-lg-for-powerline'

## Install from App Store (https://github.com/mas-cli/mas)
## This requires signin with `mas signin <your-apple-id>`
#mas '1Password', id: 443987910
#mas 'CCMenu', id: 603117688
#mas 'Dash', id: 458034879
